from io import StringIO

import numpy as np
import plotly.graph_objects as go
import pytorch_lightning as pl
import torch
import wandb
from pytorch_lightning.callbacks import Callback
from pytorch_lightning.loggers import LoggerCollection, WandbLogger

from ..model.convolution_latent_code import ColorNPM


class LogColorsCallback(Callback):
    NUM_BOARDS = 5

    def __init__(self, log_every=5):
        super().__init__()
        self.state = {"epochs": 0}
        self.log_every = log_every

    @staticmethod
    def _create_plotly_figure(x, y, z, color_values, sdf_values):
        distance = 0.05
        close_points_mask = (sdf_values <= distance) & (sdf_values >= -distance)

        x = x[close_points_mask]
        y = y[close_points_mask]
        z = z[close_points_mask]
        close_points_colors = color_values[close_points_mask]

        sub_idx = np.random.choice(np.arange(x.shape[0]), size=20000)

        x = x[sub_idx]
        y = y[sub_idx]
        z = z[sub_idx]
        close_points_colors = close_points_colors[sub_idx]

        return go.Figure(
            data=[
                go.Scatter3d(
                    x=x,
                    y=y,
                    z=z,
                    mode="markers",
                    marker=dict(size=2, color=close_points_colors, opacity=1),
                )
            ]
        )

    def _log_points(self, logger, plotly_arguments):

        if isinstance(logger, WandbLogger):
            wandb.log(
                {
                    f"colored_points_{i}": wandb.Plotly(
                        self._create_plotly_figure(x, y, z, color_values, sdf_values)
                    )
                    for i, (x, y, z, sdf_values, color_values) in enumerate(
                        plotly_arguments
                    )
                }
            )

    def on_train_epoch_end(
        self,
        trainer: "pl.Trainer",
        model: ColorNPM,
        unused=None,
    ):
        self.state["epochs"] += 1

        if self.state["epochs"] % self.log_every == 0:
            # Compute meshes
            model.eval()

            shape_latent_vectors = model.shape_latent_vectors(
                torch.LongTensor(list(range(self.NUM_BOARDS))).to(model.device)
            )
            color_latent_vectors = model.color_latent_vectors(
                torch.LongTensor(list(range(self.NUM_BOARDS))).to(model.device)
            )

            plotly_arguments = []
            for i in range(self.NUM_BOARDS):
                x, y, z, sdf_values, color_values = model.infer_single(
                    shape_latent_vectors[i, :],
                    color_latent_vectors[i, :],
                    128,
                )
                sdf_values = sdf_values.flatten()
                color_values = color_values.reshape(-1, 3)

                plotly_arguments.append((x, y, z, sdf_values, color_values))

            # Check whether we have one logger or multiple
            # and log to all loggers we have
            if isinstance(trainer.logger, LoggerCollection):
                for logger in trainer.logger:
                    self._log_points(logger, plotly_arguments)
            else:
                self._log_points(trainer.logger, plotly_arguments)

    def on_load_checkpoint(self, trainer, pl_module, callback_state):
        self.state.update(callback_state)

    def on_save_checkpoint(self, trainer, pl_module, checkpoint):
        return self.state.copy()
