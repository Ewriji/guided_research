import typing as t

import torch
import wandb
from pytorch_lightning import Trainer
from pytorch_lightning import loggers as pl_loggers

from ..callbacks import (
    LogColorsCallback,
    LogMeshesCallback,
    LogModelWightsCallback,
    LogVolumesCallback,
)
from ..data.colored_shapenet import ColoredShapeNetDataModule
from ..model.convolution_latent_code import ColorNPM
from .config import TrainingConfig


def construct_typed_config(ini_config):
    config = TrainingConfig(
        **ini_config["training"],
        **ini_config["shape_net"],
        **ini_config["tensorboard"],
        **ini_config["deep_sdf"]
    )

    return config


def train(
    config: TrainingConfig,
    resume_from_checkpoint: t.Optional[str] = None,
    run_id: t.Optional[str] = None,
):
    wandb.init(project="CNPM")

    # Create DataModule
    datamodule = ColoredShapeNetDataModule(
        batch_size=config.batch_size,
        num_workers=config.num_workers,
        num_samples_point=config.num_samples_point,
        path_to_split=(
            config.path_to_overfit_split
            if config.is_overfit
            else config.path_to_train_split
        ),
        path_to_dataset=config.path_to_dataset,
    )
    datamodule.setup()

    # Instantiate model
    model = ColorNPM(
        latent_num=len(datamodule.train_dataset),
        shape_latent_dim=config.shape_latent_code_length,
        color_latent_dim=config.color_latent_code_length,
        model_learning_rate=config.learning_rate_model,
        latent_code_learning_rate=config.learning_rate_code,
        lambda_code_regularization=config.lambda_code_regularization,
        with_shape=False,
    )

    if resume_from_checkpoint:
        print("Restoring model from checkpoint..")
        checkpoint = torch.load(resume_from_checkpoint)
        model.load_state_dict(checkpoint["model_state_dict"])

    # Create logger
    # tb_logger = pl_loggers.TensorBoardLogger(save_dir=config.logging_path)
    wb_logger = pl_loggers.WandbLogger(project="CNPM", log_model="all")
    wb_logger.watch(model)

    # Start training
    trainer = Trainer(
        gpus=config.gpus,
        check_val_every_n_epoch=config.validate_every_n,
        default_root_dir=config.resume_ckpt_path,
        logger=wb_logger,
        log_every_n_steps=1,
        max_epochs=config.max_epochs,
        callbacks=[
            LogMeshesCallback(log_every=100),
            LogColorsCallback(log_every=100),
            LogModelWightsCallback(log_every=100),
        ],
    )
    trainer.fit(model, datamodule=datamodule)
