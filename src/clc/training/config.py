import typing as t

from pydantic import BaseModel


class TrainingConfig(BaseModel):
    # Tensorboard
    logging_path: str

    # General configuration
    gpus: int
    is_overfit: bool
    batch_size: int
    resume_ckpt_path: str
    learning_rate: float
    max_epochs: int
    validate_every_n: int
    num_workers: int

    # ShapeNet configuration
    num_classes: int
    path_to_train_split: str
    path_to_overfit_split: str
    path_to_dataset: str

    # DeepSDF
    num_samples_point: int
    shape_latent_code_length: int
    color_latent_code_length: int

    learning_rate_model: float
    learning_rate_code: float

    lambda_code_regularization: float
    enforce_minmax: bool
