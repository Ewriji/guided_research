import itertools
import typing as t

import numpy as np
import pytorch_lightning as pl
import torch
from skimage import measure
from torch import nn
from torch.nn import Parameter


class Downscale(nn.Module):
    def __init__(self, in_channels: int, out_channels: int) -> None:
        super().__init__()

        self.block = nn.Sequential(
            nn.Conv3d(in_channels, out_channels, kernel_size=3, stride=2, padding=1),
            nn.BatchNorm3d(out_channels),
            nn.ReLU(),
        )

    def forward(self, voxel_grid: torch.Tensor) -> torch.Tensor:
        return self.block(voxel_grid)


class ResBlock(nn.Module):
    def __init__(self, channels: int) -> None:
        super().__init__()

        self.block_1 = nn.Sequential(
            nn.Conv3d(channels, channels, kernel_size=3, stride=2, padding=1),
            nn.BatchNorm3d(channels),
            nn.ReLU(),
        )

        self.block_2 = nn.Sequential(
            nn.Conv3d(channels, channels, kernel_size=3, stride=2, padding=1),
            nn.BatchNorm3d(channels),
        )

        self.out = nn.ReLU()

    def forward(self, voxel_grid: torch.Tensor) -> torch.Tensor:
        x = self.block_1(voxel_grid)
        x = voxel_grid + x
        x = self.block_2(x)
        x = self.out(x)

        return x


class DownscaleBlock(nn.Module):
    def __init__(self, in_channels: int, out_channels: int) -> None:
        super().__init__()

        self.block = nn.Sequential(
            Downscale(in_channels, out_channels),
            ResBlock(out_channels),
        )

    def forward(self, voxel_grid: torch.Tensor) -> torch.Tensor:
        return self.block(voxel_grid)


class ConvolutionLatentCode(pl.LightningModule):
    def __init__(
        self,
        latent_dim: int,
        *,
        model_learning_rate: float,
    ):
        super().__init__()

        self.model_learning_rate = model_learning_rate
        self.latent_dim = latent_dim

        self._init_losses()
        self._init_layers()

    def _init_losses(self) -> None:
        self.loss_criterion = torch.nn.L1Loss()

    def _init_layers(self) -> None:
        self.encoder = nn.Sequential(
            DownscaleBlock(1, 8),
            DownscaleBlock(8, 16),
            DownscaleBlock(16, 32),
            DownscaleBlock(32, 64),
            DownscaleBlock(64, 128),
            Downscale(128, 256),
            nn.Flatten(),
            nn.Conv1d(256, 4**3, kernel_size=2 * self.latent_dim),
            nn.LeakyReLU(),
            nn.Conv1d(self.latent_dim * 2, self.latent_dim),
        )

    def forward(
        self,
        voxel_grid: torch.Tensor,
    ) -> torch.Tensor:
        return self.encoder(voxel_grid)

    # REGION PYTORCH LIGHTNING
    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters())
        scheduler = torch.optim.lr_scheduler.StepLR(
            optimizer,
            step_size=500,
            gamma=0.5,
        )

        return [optimizer], [scheduler]

    def training_step(self, batch: t.Dict[str, torch.Tensor], batch_idx):
        # Calculate number of samples per batch
        # (= number of shapes in batch * number of points per shape)
        batch_size = batch["points"].shape[0]
        num_points_per_batch = batch_size * batch["points"].shape[1]

        # Get shape latent codes corresponding to batch shapes
        batch_shape_latent_vectors = (
            self.shape_latent_vectors(batch["indices"])
            .unsqueeze(1)
            .expand(-1, batch["points"].shape[1], -1)
            .reshape((num_points_per_batch, self.shape_latent_dim))
        )

        # Get color latent codes corresponding to batch shapes
        batch_color_latent_vectors = (
            self.color_latent_vectors(batch["indices"])
            .unsqueeze(1)
            .expand(-1, batch["points"].shape[1], -1)
            .reshape((num_points_per_batch, self.color_latent_dim))
        )

        # Reshape points and sdf for forward pass
        points = batch["points"].reshape((num_points_per_batch, 3))
        sdf = batch["sdf"].reshape((num_points_per_batch, 1))
        colors = batch["color"].reshape((num_points_per_batch, 3))

        if self.enforce_minmax:
            sdf = torch.clamp(sdf, -0.1, 0.1)

        # Perform forward pass
        predicted_sdf, predicted_color = self.forward(
            points,
            batch_shape_latent_vectors,
            batch_color_latent_vectors,
        )

        # Compute losses
        loss, shape_loss, color_loss = self.compute_loss(
            predicted_sdf,
            sdf,
            batch_shape_latent_vectors,
            predicted_color,
            colors,
            batch_color_latent_vectors,
        )
        if self.with_color:
            self.log("color_loss", color_loss, on_epoch=True, batch_size=batch_size)

        if self.with_shape:
            self.log("shape_loss", shape_loss, on_epoch=True, batch_size=batch_size)

        self.log("train_loss", loss, on_epoch=True, batch_size=batch_size)

        # Make a scheduler step
        if self.trainer.is_last_batch:
            self.lr_schedulers().step()

        return loss

    def transfer_batch_to_device(
        self,
        batch: t.Dict[str, torch.Tensor],
        device: torch.device,
        dataloader_idx: int,
    ) -> t.Dict[str, torch.Tensor]:

        batch["points"] = batch["points"].to(device)
        batch["sdf"] = batch["sdf"].to(device)
        batch["indices"] = batch["indices"].to(device)
        batch["color"] = batch["color"].to(device)

        return batch

    # ENDREGION
