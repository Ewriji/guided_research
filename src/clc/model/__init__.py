from .convolution_latent_code import ColorNPM

__all__ = ["ColorNPM"]
