import typing as t

import numpy as np
import pandas as pd
import pytorch_lightning as pl
import torch
from sklearn.neighbors import KDTree
from torch.utils.data import DataLoader, Dataset, random_split


class SnowDataset(Dataset):
    """
    Dataset for loading snow samples from csv file
    """

    def __init__(
        self,
        dataset_path: str,
        kn: int = 64,
        patch_nums=128,
    ):
        self.patch_nums = patch_nums
        self.kn = kn
        self.data = pd.read_csv(dataset_path)
        self.num_scenes = len(self.data.scene_id.unique())

    def __getitem__(self, index):
        """
        :param index: Index of the scene from which we want to sample data
        :return: A dictionary with following entries:
                 - scene_ids: Id of scene
                 - labels: Corresponding target labels
                 - values: Tensor of shape [n, 5], e.g [[x, y, z, intensity, ring], ...]
        """
        # extract data
        scene_data = self.data[self.data.scene_id == index].values
        coordinates = scene_data[:, :3]
        features = scene_data[:, :5]
        labels = scene_data[:, 6]

        # construct flann
        tree = KDTree(coordinates)
        sampled_idx = np.random.choice(coordinates.shape[0], size=self.patch_nums)
        nn_ids = tree.query(
            coordinates[sampled_idx], k=self.kn, return_distance=False
        ).flatten()

        return {
            "scene_ids": index,
            "labels": torch.tensor(
                labels[nn_ids].reshape(-1, self.kn), dtype=torch.int64
            ),
            "values": torch.tensor(
                features[nn_ids].reshape(-1, self.kn, 5), dtype=torch.float
            ),
        }

    def __len__(self):
        """
        :return: length of the dataset
        """

        return self.num_scenes


class SnowDatamodule(pl.LightningDataModule):
    """
    Pytorch lightning Datamodule with Snow dataset
    """

    train_dataset: SnowDataset
    val_dataset: SnowDataset

    def __init__(
        self,
        batch_size: int,
        num_workers: int,
        *,
        dataset_path: str,
        kn: int = 64,
        patch_nums=128,
    ):
        super().__init__()

        self.patch_nums = patch_nums
        self.kn = kn
        self.num_workers = num_workers
        self.batch_size = batch_size
        self.dataset_path = dataset_path

    def setup(self, stage: t.Optional[str] = None) -> None:
        dataset = SnowDataset(
            dataset_path=self.dataset_path,
            kn=self.kn,
            patch_nums=self.patch_nums,
        )
        self.train_dataset, self.val_dataset = random_split(
            dataset, [260, len(dataset) - 260]
        )

    def train_dataloader(self):
        return DataLoader(
            self.train_dataset,
            batch_size=self.batch_size,
            num_workers=self.num_workers,
            shuffle=True,
            pin_memory=True,
        )

    def val_dataloader(self):
        return DataLoader(
            self.val_dataset,
            batch_size=50,
            num_workers=self.num_workers,
            shuffle=False,
        )
