import typing as t

import pytorch_lightning as pl
import torch
import torch.nn as nn
import numpy as np
import torch.utils.data
from sklearn.metrics import roc_auc_score


class TNet(nn.Module):
    def __init__(self, k=3):
        super(TNet, self).__init__()
        self.k = k
        self.net = nn.Sequential(
            # layer 1
            nn.Conv1d(self.k, 128, 1),
            nn.BatchNorm1d(128),
            nn.ReLU(),
            # layer 2
            nn.Conv1d(128, 512, 1),
            nn.BatchNorm1d(512),
            nn.ReLU(),
            # layer 3
            nn.Conv1d(512, 1024, 1),
            nn.BatchNorm1d(1024),
            nn.ReLU(),
        )
        self.maxpool = nn.AdaptiveMaxPool1d(1)
        self.aggregation = nn.Sequential(
            # layer 4
            nn.Linear(1024, 512),
            nn.BatchNorm1d(512),
            nn.ReLU(),
            # layer 5
            nn.Linear(512, 256),
            nn.BatchNorm1d(256),
            nn.ReLU(),
        )

        self.out = nn.Linear(256, self.k * self.k)

    def forward(self, x):
        batch_size = x.size()[0]
        init = nn.Parameter(torch.eye(self.k, device=x.device).repeat(batch_size, 1, 1))

        x = self.net(x)
        x = self.maxpool(x).squeeze(-1)
        x = self.aggregation(x)
        x = self.out(x).reshape(-1, self.k, self.k) + init

        return x


class PointNet(pl.LightningModule):
    def __init__(
        self,
        channel: int,
        part_num: int,
        learning_rate: float = 0.003,
        betas: t.Tuple[float, float] = (0.9, 0.999),
    ):
        super(PointNet, self).__init__()

        self.learning_rate = learning_rate
        self.betas = betas
        self.part_num = part_num

        self.t_net = TNet(k=3)
        self.layer_1 = nn.Sequential(
            nn.Conv1d(channel, 64, 1),
            nn.BatchNorm1d(64),
            nn.ReLU(),
        )
        self.layer_2 = nn.Sequential(
            nn.Conv1d(64, 128, 1),
            nn.BatchNorm1d(128),
            nn.ReLU(),
        )
        self.layer_3 = nn.Sequential(
            nn.Conv1d(128, 128, 1),
            nn.BatchNorm1d(128),
            nn.ReLU(),
        )

        self.feature_t_net = TNet(k=128)

        self.layer_4 = nn.Sequential(
            nn.Conv1d(128, 512, 1),
            nn.BatchNorm1d(512),
            nn.ReLU(),
        )

        self.layer_5 = nn.Sequential(
            nn.Conv1d(512, 2048, 1),
            nn.BatchNorm1d(2048),
        )

        self.segmentation = nn.Sequential(
            nn.Conv1d(4928, 256, 1),
            nn.BatchNorm1d(256),
            nn.ReLU(),
            nn.Conv1d(256, 256, 1),
            nn.BatchNorm1d(256),
            nn.ReLU(),
            nn.Conv1d(256, 128, 1),
            nn.BatchNorm1d(128),
            nn.ReLU(),
            nn.Conv1d(128, self.part_num, 1),
        )
        self.maxpool = nn.AdaptiveMaxPool1d(1)
        self.logsoftmax = nn.LogSoftmax(1)
        self.loss = nn.NLLLoss()

    def forward(self, x):
        B, N, _ = x.shape

        point_cloud, feature = x.split(3, dim=2)
        trans = self.t_net(point_cloud.transpose(2, 1))

        point_cloud = torch.bmm(point_cloud, trans)
        point_cloud = torch.cat([point_cloud, feature], dim=2)
        point_cloud = point_cloud.transpose(2, 1)

        out1 = self.layer_1(point_cloud)
        out2 = self.layer_2(out1)
        out3 = self.layer_3(out2)

        trans_features = self.feature_t_net(out3)
        f_transformed = torch.bmm(out3.transpose(2, 1), trans_features)
        f_transformed = f_transformed.transpose(2, 1)

        out4 = self.layer_4(f_transformed)
        out5 = self.layer_5(out4)

        global_feature = self.maxpool(out5)
        global_feature = global_feature.squeeze(-1)
        global_feature = global_feature.view(-1, 2048, 1).repeat(1, 1, N)

        concat = torch.cat([global_feature, out1, out2, out3, out4, out5], 1)
        seg = self.segmentation(concat)
        seg = seg.transpose(2, 1).reshape(-1, self.part_num)
        seg = self.logsoftmax(seg)
        seg = seg.view(B, N, self.part_num)

        return seg, trans, trans_features

    @staticmethod
    def _transform_loss(trans):
        d = trans.size()[1]
        I = torch.eye(d)[None, :, :].to(trans.device)

        loss = torch.mean(
            torch.norm(torch.bmm(trans, trans.transpose(2, 1)) - I, dim=(1, 2))
        )
        return loss

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(
            self.parameters(), lr=self.learning_rate, betas=self.betas
        )
        scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=20, gamma=0.5)

        return [optimizer], [scheduler]

    def training_step(self, batch: t.Dict[str, t.Any], batch_idx):
        # unpack batch and glue patches across batches
        labels = batch["labels"].reshape(-1)
        values = batch["values"].reshape(-1, *batch["values"].shape[2:])

        # make a prediction
        seg, _, trans_features = self.forward(values)
        seg = seg.reshape(-1, seg.shape[-1])

        # compute losses
        seg_loss = self.loss(seg, labels)
        trans_loss = self._transform_loss(trans_features) * 0.001

        loss = seg_loss + trans_loss

        # do logging :)
        self.log("loss", loss)
        self.log("seg_loss", seg_loss)
        self.log("trans_loss", trans_loss)

        # Make a scheduler step
        if self.trainer and self.trainer.is_last_batch:
            self.lr_schedulers().step()

        return loss

    def validation_step(self, batch: t.Dict[str, t.Any], batch_idx):
        # unpack batch and glue patches across batches
        labels = batch["labels"].reshape(-1)
        values = batch["values"].reshape(-1, *batch["values"].shape[2:])

        seg, _, _ = self.forward(values)
        seg = seg.reshape(-1, seg.shape[-1])

        # nll loss
        loss = self.loss(seg, labels)
        self.log("val_loss", loss, on_epoch=True)

        # roc auc
        labels = labels.cpu().detach().numpy()
        if len(np.unique(labels)) == 2:
            seg_pred = seg.data.max(1)[1].cpu().detach().numpy()

            score = roc_auc_score(labels, seg_pred)

            self.log("roc_auc_score", score, on_epoch=True)

        return loss

    def transfer_batch_to_device(
        self,
        batch: t.Dict[str, t.Any],
        device: torch.device,
        dataloader_idx: int,
    ) -> t.Dict[str, torch.Tensor]:
        batch["labels"] = batch["labels"].to(device)
        batch["values"] = batch["values"].to(device)

        return batch
