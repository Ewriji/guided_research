import typing as t

from pytorch_lightning import Trainer
from pytorch_lightning import loggers as pl_loggers

from ..data.shape_implicit import ShapeImplicitDataModule
from ..model.deepsdf import DeepSDF
from .config import TrainingConfig


def construct_typed_config(ini_config):
    config = TrainingConfig(
        **ini_config["training"],
        **ini_config["shape_net"],
        **ini_config["tensorboard"],
        **ini_config["deep_sdf"]
    )

    return config


def train(
    config: TrainingConfig,
    resume_from_checkpoint: t.Optional[str] = None,
    run_id: t.Optional[str] = None,
):
    # Create DataModule
    datamodule = ShapeImplicitDataModule(
        batch_size=config.batch_size,
        num_workers=config.num_workers,
        num_samples_point=config.num_samples_point,
        path_to_split=(
            config.path_to_overfit_split
            if config.is_overfit
            else config.path_to_train_split
        ),
        path_to_dataset=config.path_to_dataset,
    )
    datamodule.setup()

    # Instantiate model
    model = DeepSDF(
        len(datamodule.train_dataset),
        config.latent_code_length,
        model_learning_rate=config.learning_rate_model,
        latent_code_learning_rate=config.learning_rate_code,
        lambda_code_regularization=config.lambda_code_regularization,
    )

    # Create logger
    tb_logger = pl_loggers.TensorBoardLogger(save_dir=config.logging_path)
    wb_logger = pl_loggers.WandbLogger()

    # Start training
    trainer = Trainer(
        gpus=config.gpus,
        check_val_every_n_epoch=config.validate_every_n,
        default_root_dir=config.resume_ckpt_path,
        resume_from_checkpoint=resume_from_checkpoint,
        logger=tb_logger,
        log_every_n_steps=1,
        max_epochs=config.max_epochs,
    )
    trainer.fit(model, datamodule=datamodule)
