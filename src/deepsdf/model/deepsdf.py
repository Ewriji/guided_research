import itertools
import typing as t

import numpy as np
import pytorch_lightning as pl
import torch
from skimage import measure
from torch import nn
from torch.nn import Parameter
from torch.nn.utils import weight_norm


class DeepSDF(pl.LightningModule):
    def __init__(
        self,
        latent_num: int,
        latent_dim: int,
        enforce_minmax: bool = True,
        *,
        model_learning_rate: float,
        latent_code_learning_rate: float,
        lambda_code_regularization: float
    ):

        super(DeepSDF, self).__init__()

        self.lambda_code_regularization = lambda_code_regularization
        self.latent_code_learning_rate = latent_code_learning_rate
        self.model_learning_rate = model_learning_rate
        self.latent_num = latent_num
        self.latent_dim = latent_dim
        self.enforce_minmax = enforce_minmax

        self._init_losses()
        self._init_layers()

    def _init_losses(self) -> None:
        self.loss_criterion = torch.nn.L1Loss()
        self.loss_criterion.to(self.device)

    def _init_layers(self) -> None:

        self.latent_vectors = torch.nn.Embedding(
            self.latent_num,
            self.latent_dim,
            max_norm=1.0,
        )

        self.decoder_1 = nn.Sequential(
            weight_norm(nn.Linear(self.latent_dim + 3, 512)),
            nn.ReLU(),
            nn.Dropout(0.2),
            weight_norm(nn.Linear(512, 512)),
            nn.ReLU(),
            nn.Dropout(0.2),
            weight_norm(nn.Linear(512, 512)),
            nn.ReLU(),
            nn.Dropout(0.2),
            weight_norm(nn.Linear(512, 512 - self.latent_dim - 3)),
            nn.ReLU(),
            nn.Dropout(0.2),
        )
        self.decoder_2 = nn.Sequential(
            weight_norm(nn.Linear(512, 512)),
            nn.ReLU(),
            nn.Dropout(0.2),
            weight_norm(nn.Linear(512, 512)),
            nn.ReLU(),
            nn.Dropout(0.2),
            weight_norm(nn.Linear(512, 512)),
            nn.ReLU(),
            nn.Dropout(0.2),
        )
        self.output_layer = weight_norm(nn.Linear(512, 1))
        self.tanh = nn.Tanh()

    def forward(self, x: torch.Tensor) -> torch.Tensor:

        skip_connection = x
        x = self.decoder_1(x)
        x = torch.cat([x, skip_connection], dim=-1)
        x = self.decoder_2(x)
        x = self.output_layer(x)
        x = self.tanh(x)

        return x

    def infer_single(self, latent_code, grid_resolution, clamp: bool = True):

        x_range = y_range = z_range = np.linspace(-1.0, 1.0, grid_resolution)
        grid_x, grid_y, grid_z = np.meshgrid(x_range, y_range, z_range, indexing="ij")
        grid_x, grid_y, grid_z = grid_x.flatten(), grid_y.flatten(), grid_z.flatten()

        stacked = (
            torch.from_numpy(
                np.hstack(
                    (
                        grid_x[:, np.newaxis],
                        grid_y[:, np.newaxis],
                        grid_z[:, np.newaxis],
                    )
                )
            )
            .float()
            .to(self.device)
        )
        stacked_split = torch.split(stacked, 32**3, dim=0)
        sdf_values = []

        for points in stacked_split:
            latent_codes = latent_code.unsqueeze(0).expand(points.shape[0], -1)
            sdf = self(torch.cat([latent_codes, points], 1))
            if clamp:
                sdf = torch.clamp(sdf, -0.1, 0.1)
            sdf_values.append(sdf.detach().cpu())

        sdf_values = (
            torch.cat(sdf_values, dim=0)
            .numpy()
            .reshape((grid_resolution, grid_resolution, grid_resolution))
            .copy()
        )

        return grid_x, grid_y, grid_z, sdf_values

    @staticmethod
    def compute_mesh(sdf_values):
        if 0 < sdf_values.min() or 0 > sdf_values.max():
            mesh = None
            empty = True
        else:
            try:
                mesh = measure.marching_cubes(sdf_values)
                empty = False
            except ValueError:
                mesh = None
                empty = True

        return mesh, empty

    @staticmethod
    def _extract_vertices_and_faces(
        mesh, flip_axes=False
    ) -> t.Tuple[np.ndarray, np.ndarray]:

        vertices, faces = mesh[:2]
        if flip_axes:
            vertices[:, 2] = vertices[:, 2] * -1
            vertices[:, [0, 1, 2]] = vertices[:, [0, 2, 1]]

        return vertices.astype(np.float32), faces.astype(np.int32)

    @property
    def model_parameters(self) -> t.Iterator[Parameter]:
        model_parameter = [
            self.decoder_1.parameters(),
            self.decoder_2.parameters(),
            self.output_layer.parameters(),
        ]

        return itertools.chain(*model_parameter)

    @property
    def latent_vectors_parameters(self) -> t.Iterator[Parameter]:
        return self.latent_vectors.parameters()

    def compute_loss(self, predicted_sdf, sdf, batch_latent_vectors):
        # Truncate predicted sdf between -0.1 and 0.1
        if self.enforce_minmax:
            predicted_sdf = torch.clamp(predicted_sdf, -0.1, 0.1)

        # Compute loss
        loss = self.loss_criterion(predicted_sdf, sdf)

        # regularize latent codes
        code_regularization = (
            torch.mean(torch.norm(batch_latent_vectors, dim=1))
            * self.lambda_code_regularization
        )
        if self.current_epoch > 100:
            loss = loss + code_regularization

        return loss

    # REGION PYTORCH LIGHTNING
    def configure_optimizers(self):

        optimizer = torch.optim.Adam(
            [
                {"params": self.model_parameters, "lr": self.model_learning_rate},
                {
                    "params": self.latent_vectors_parameters,
                    "lr": self.latent_code_learning_rate,
                },
            ]
        )
        scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=50, gamma=0.5)

        return [optimizer], [scheduler]

    def training_step(self, batch: t.Dict[str, torch.Tensor], batch_idx):
        # Calculate number of samples per batch
        # (= number of shapes in batch * number of points per shape)
        batch_size = batch["points"].shape[0]
        num_points_per_batch = batch_size * batch["points"].shape[1]

        # Get latent codes corresponding to batch shapes
        # expand so that we have an appropriate latent vector per sdf sample
        batch_latent_vectors = (
            self.latent_vectors(batch["indices"])
            .unsqueeze(1)
            .expand(-1, batch["points"].shape[1], -1)
            .reshape((num_points_per_batch, self.latent_dim))
        )

        # Reshape points and sdf for forward pass
        points = batch["points"].reshape((num_points_per_batch, 3))
        sdf = batch["sdf"].reshape((num_points_per_batch, 1))

        if self.enforce_minmax:
            sdf = torch.clamp(sdf, -0.1, 0.1)

        # Perform forward pass
        input = torch.cat([batch_latent_vectors, points], dim=-1)
        predicted_sdf = self.forward(input)

        # Truncate predicted sdf between -0.1 and 0.1
        loss = self.compute_loss(
            predicted_sdf,
            sdf,
            batch_latent_vectors,
        )

        self.log("train_loss", loss, on_epoch=True, batch_size=batch_size)

        # Make a scheduler step
        if self.trainer.is_last_batch:
            self.lr_schedulers().step()

        return loss

    def transfer_batch_to_device(
        self,
        batch: t.Dict[str, torch.Tensor],
        device: torch.device,
        dataloader_idx: int,
    ) -> t.Dict[str, torch.Tensor]:

        batch["points"] = batch["points"].to(device)
        batch["sdf"] = batch["sdf"].to(device)
        batch["indices"] = batch["indices"].to(device)

        return batch

    # ENDREGION
