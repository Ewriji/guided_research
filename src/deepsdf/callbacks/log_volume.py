import pytorch_lightning as pl
import torch
import wandb
from pytorch_lightning.callbacks import Callback
from pytorch_lightning.loggers import LoggerCollection, WandbLogger

from src.deepsdf.model.deepsdf import DeepSDF
from src.util.visualization import visualize_sdf


class LogVolumesCallback(Callback):
    NUM_BOARDS = 5

    def __init__(self, log_every=5):
        super().__init__()
        self.state = {"epochs": 0}
        self.log_every = log_every

    def _log_volumes(self, sdf_values, logger, step):
        if isinstance(logger, WandbLogger):
            wandb.log(
                {
                    f"volume_{i}": wandb.Plotly(visualize_sdf(x, y, z, sdf_value))
                    for i, (x, y, z, sdf_value) in enumerate(sdf_values)
                }
            )

    def on_train_epoch_end(
        self,
        trainer: "pl.Trainer",
        model: DeepSDF,
        unused=None,
    ):
        self.state["epochs"] += 1

        if self.state["epochs"] % self.log_every == 0:
            model.eval()
            # Compute meshes
            latent_vectors_for_vis = model.latent_vectors(
                torch.LongTensor(list(range(self.NUM_BOARDS))).to(model.device)
            )

            sdf_values = []
            for i in range(latent_vectors_for_vis.shape[0]):
                x, y, z, sdf_value = model.infer_single(
                    latent_vectors_for_vis[i, :],
                    64,
                    clamp=False,
                )
                sdf_values.append((x, y, z, sdf_value.flatten()))

            if isinstance(trainer.logger, LoggerCollection):
                for logger in trainer.logger:
                    self._log_volumes(sdf_values, logger, step=model.global_step)
            else:
                self._log_volumes(sdf_values, trainer.logger, step=model.global_step)

    def on_load_checkpoint(self, trainer, pl_module, callback_state):
        self.state.update(callback_state)

    def on_save_checkpoint(self, trainer, pl_module, checkpoint):
        return self.state.copy()
