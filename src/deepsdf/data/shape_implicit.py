import typing as t
from pathlib import Path

import numpy as np
import pytorch_lightning as pl
import torch
import trimesh
from torch.utils.data import DataLoader, Dataset

from src.util.misc import remove_nans


class ShapeImplicitDataset(Dataset):
    """
    Dataset for loading deep sdf training samples
    """

    def __init__(self, dataset_path: str, num_sample_points: int, split: str):
        """
        :param num_sample_points: number of points to sample for sdf values per shape
        :param split: one of 'train', 'val' or 'overfit' - for training,
                      validation or overfitting split
        """
        super().__init__()

        self.dataset_path = Path(dataset_path)
        self.num_sample_points = num_sample_points
        self.items = (
            Path(split).read_text().splitlines()
        )  # keep track of shape identifiers based on split

    def __getitem__(self, index):
        """
        PyTorch requires you to provide a getitem implementation for your dataset.
        :param index: index of the dataset sample that will be returned
        :return: a dictionary of sdf data corresponding to the shape. In particular, this dictionary has keys
                 "name", shape_identifier of the shape
                 "indices": index parameter
                 "points": a num_sample_points x 3  pytorch float32 tensor containing sampled point coordinates
                 "sdf", a num_sample_points x 1 pytorch float32 tensor containing sdf values for the sampled points
        """

        # get shape_id at index
        item = self.items[index]

        # get path to sdf data
        sdf_samples_path = self.dataset_path / item / "sdf.npz"

        # read points and their sdf values from disk
        sdf_samples = self.get_sdf_samples(sdf_samples_path)

        points = sdf_samples[:, :3]
        sdf = sdf_samples[:, 3:]

        return {
            "name": item,  # identifier of the shape
            "indices": index,  # index parameter
            "points": points,  # points, a tensor with shape num_sample_points x 3
            "sdf": sdf,  # sdf values, a tensor with shape num_sample_points x 1
        }

    def __len__(self):
        """
        :return: length of the dataset
        """

        return len(self.items)

    def get_sdf_samples(self, path_to_sdf):
        """
        Utility method for reading an sdf file; the SDF file for a shape contains
        a number of points, along with their sdf values

        :param path_to_sdf: path to sdf file
        :return: a pytorch float32 torch tensor of shape (num_sample_points, 4)
                 with each row being [x, y, z, sdf_value at xyz]
        """
        N = self.num_sample_points // 2

        npz = np.load(path_to_sdf)
        pos_tensor = remove_nans(torch.from_numpy(npz["pos"]))
        neg_tensor = remove_nans(torch.from_numpy(npz["neg"]))

        perm = torch.randperm(pos_tensor.size(0))
        idx = perm[:N]
        samples_pos = pos_tensor[idx]

        perm = torch.randperm(neg_tensor.size(0))
        idx = perm[: self.num_sample_points - N]
        samples_neg = neg_tensor[idx]

        return torch.cat([samples_pos, samples_neg], dim=0)

    def get_mesh(self, shape_id):
        """
        Loading a mesh from the shape with identifier

        :param shape_id: shape identifier for ShapeNet object
        :return: trimesh object representing the mesh
        """
        return trimesh.load(self.dataset_path / shape_id / "mesh.obj", force="mesh")

    def get_all_sdf_samples(self, shape_id):
        """
        Loading all points and their sdf values

        :param shape_id: shape identifier for ShapeNet object
        :return: two torch float32 tensors, a Nx3 tensor containing point coordinates,
                 and Nx1 tensor containing their sdf values
        """
        npz = np.load(self.dataset_path / shape_id / "sdf.npz")
        pos_tensor = remove_nans(torch.from_numpy(npz["pos"]))
        neg_tensor = remove_nans(torch.from_numpy(npz["neg"]))

        samples = torch.cat([pos_tensor, neg_tensor], 0)

        points = samples[:, :3]
        sdf = samples[:, 3:]

        return points, sdf


class EmptyDataset(Dataset):
    """
    Mock dataset for DeppSDF empty validation step
    """

    def __init__(self):
        super().__init__()

    def __getitem__(self, index):
        return {
            "name": torch.tensor([1]),
            "indices": torch.tensor([1]),
            "points": torch.tensor([1]),
            "sdf": torch.tensor([1]),
        }

    def __len__(self):
        return 10


class ShapeImplicitDataModule(pl.LightningDataModule):
    train_dataset: ShapeImplicitDataset
    val_dataset: EmptyDataset

    def __init__(
        self,
        batch_size: int,
        num_workers: int,
        *,
        num_samples_point: int,
        path_to_split: str,
        path_to_dataset: str,
    ):
        super().__init__()
        self.num_samples_point = num_samples_point
        self.path_to_dataset = path_to_dataset
        self.path_to_split = path_to_split
        self.batch_size = batch_size
        self.num_workers = num_workers

    def setup(self, stage: t.Optional[str] = None) -> None:
        self.train_dataset = ShapeImplicitDataset(
            self.path_to_dataset, self.num_samples_point, self.path_to_split
        )

        self.val_dataset = EmptyDataset()

    def train_dataloader(self):
        return DataLoader(
            self.train_dataset,
            batch_size=self.batch_size,
            num_workers=self.num_workers,
            shuffle=True,
            pin_memory=True,
        )

    def val_dataloader(self):
        return DataLoader(self.val_dataset, batch_size=1, num_workers=1, shuffle=False)
