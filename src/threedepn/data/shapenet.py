import json
import typing as t
from pathlib import Path

import numpy as np
import pytorch_lightning as pl
import torch
from torch.utils.data import DataLoader


class ShapeNetDataset(torch.utils.data.Dataset):
    def __init__(
        self,
        path_to_sdf: str,
        path_to_df: str,
        path_to_class_name_mapping: str,
        path_to_split: str,
        num_classes: int,
    ):
        super().__init__()
        self.truncation_distance = 3
        self.num_classes = num_classes

        self.dataset_sdf_path = Path(path_to_sdf)  # path to voxel data
        self.dataset_df_path = Path(path_to_df)  # path to voxel data
        self.class_name_mapping = json.loads(
            Path(path_to_class_name_mapping).read_text()
        )  # mapping for ShapeNet ids -> names

        self.classes = sorted(self.class_name_mapping.keys())
        self.items = (
            Path(path_to_split).read_text().splitlines()
        )  # keep track of shapes based on split

    def __getitem__(self, index):
        sdf_id, df_id = self.items[index].split(" ")

        input_sdf = self.get_shape_sdf(sdf_id)
        target_df = self.get_shape_df(df_id)

        # Apply truncation to sdf and df
        # Stack (distances, sdf sign) for the input sdf
        # Log-scale target df
        sdf_trunc = np.clip(
            input_sdf, -self.truncation_distance, self.truncation_distance
        )
        sign = np.where(sdf_trunc < 0, -1, 1)
        sdf_trunc = np.stack([np.abs(sdf_trunc), sign], axis=0).astype(np.float32)

        # df_trunc = np.minimum(target_df, self.truncation_distance)
        df_log = np.log(target_df + 1)

        return {
            "name": f"{sdf_id}-{df_id}",
            "input_sdf": sdf_trunc,
            "target_df": df_log,
        }

    def __len__(self):
        return len(self.items)

    def get_shape_sdf(self, shapenet_id):
        shape = np.fromfile(
            self.dataset_sdf_path / (shapenet_id + ".sdf"),
            dtype=np.int64,
            count=3,
        )
        sdf = np.fromfile(
            self.dataset_sdf_path / (shapenet_id + ".sdf"),
            dtype=np.single,
            offset=24,
        )

        return np.reshape(sdf, shape)

    def get_shape_df(self, shapenet_id):
        shape = np.fromfile(
            self.dataset_df_path / (shapenet_id + ".df"),
            dtype=np.int64,
            count=3,
        )
        df = np.fromfile(
            self.dataset_df_path / (shapenet_id + ".df"),
            dtype=np.single,
            offset=24,
        )
        return np.reshape(df, shape)


class ShapeNetDataModule(pl.LightningDataModule):
    train_dataset: ShapeNetDataset
    val_dataset: ShapeNetDataset

    def __init__(
        self,
        batch_size: int,
        num_workers: int,
        num_classes: int,
        *,
        path_to_split: str,
        path_to_sdf: str,
        path_to_df: str,
        path_to_class_name_mapping: str,
    ):
        super().__init__()
        self.path_to_class_name_mapping = path_to_class_name_mapping
        self.num_classes = num_classes
        self.path_to_df = path_to_df
        self.path_to_sdf = path_to_sdf
        self.path_to_split = path_to_split
        self.batch_size = batch_size
        self.num_workers = num_workers

    def setup(self, stage: t.Optional[str] = None) -> None:
        self.train_dataset = ShapeNetDataset(
            self.path_to_sdf,
            self.path_to_df,
            self.path_to_class_name_mapping,
            self.path_to_split,
            self.num_classes,
        )
        self.val_dataset = ShapeNetDataset(
            self.path_to_sdf,
            self.path_to_df,
            self.path_to_class_name_mapping,
            self.path_to_split,
            self.num_classes,
        )

    def train_dataloader(self):
        return DataLoader(
            self.train_dataset,
            batch_size=self.batch_size,
            num_workers=self.num_workers,
            shuffle=True,
            pin_memory=True,
        )

    def val_dataloader(self):
        return DataLoader(
            self.val_dataset,
            batch_size=self.batch_size,
            num_workers=self.num_workers,
            shuffle=False,
            pin_memory=True,
        )
