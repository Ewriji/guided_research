import typing as t

from pytorch_lightning import Trainer
from pytorch_lightning import loggers as pl_loggers

from src.threedepn.data.shapenet import ShapeNetDataModule
from src.threedepn.model.threedepn import ThreeDEPN
from src.threedepn.training.config import TrainingConfig


def construct_typed_config(ini_config):
    config = TrainingConfig(
        **ini_config["training"],
        **ini_config["shape_net"],
        **ini_config["tensorboard"],
    )

    return config


def train(
    config: TrainingConfig,
    resume_from_checkpoint: t.Optional[str] = None,
    run_id: t.Optional[str] = None,
):
    """
    Function for training ThreeDEPN on ShapeNet
    """

    # Create DataModule
    datamodule = ShapeNetDataModule(
        batch_size=config.batch_size,
        num_workers=config.num_workers,
        num_classes=config.num_classes,
        path_to_split=(
            config.path_to_overfit_split
            if config.is_overfit
            else config.path_to_train_split
        ),
        path_to_sdf=config.path_to_sdf,
        path_to_df=config.path_to_df,
        path_to_class_name_mapping=config.path_to_class_name_mapping,
    )

    # Instantiate model
    model = ThreeDEPN(learning_rate=config.learning_rate)

    # Create logger
    tb_logger = pl_loggers.TensorBoardLogger(save_dir=config.logging_path)

    # Start training
    trainer = Trainer(
        gpus=config.gpus,
        check_val_every_n_epoch=config.validate_every_n,
        default_root_dir=config.resume_ckpt_path,
        resume_from_checkpoint=resume_from_checkpoint,
        logger=tb_logger,
        max_epochs=config.max_epochs,
    )
    trainer.fit(model, datamodule=datamodule)
