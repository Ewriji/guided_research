import typing as t

from pydantic import BaseModel


class TrainingConfig(BaseModel):
    # Tensorboard
    logging_path: str

    # General configuration
    gpus: int
    is_overfit: str
    batch_size: int
    resume_ckpt_path: str
    learning_rate: float
    max_epochs: int
    validate_every_n: int
    num_workers: int

    # ShapeNet configuration
    num_classes: int
    path_to_train_split: str
    path_to_overfit_split: str
    path_to_sdf: str
    path_to_df: str
    path_to_class_name_mapping: str
