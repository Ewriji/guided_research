import typing as t

import numpy as np
import pytorch_lightning as pl
import torch
from skimage import measure
from torch import nn


class ThreeDEPN(pl.LightningModule):
    def __init__(self, num_features: int = 80, *, learning_rate: float = 1e-3):
        super(ThreeDEPN, self).__init__()

        self.learning_rate = learning_rate

        self.num_features = num_features

        self._init_losses()
        self._init_layers()

    def _init_losses(self) -> None:
        self.mae = torch.nn.L1Loss()
        self.smooth_mae = torch.nn.SmoothL1Loss()

    def _init_layers(self) -> None:
        # 4 Encoder layers
        self.leaky_relu = nn.LeakyReLU(0.2, inplace=True)
        self.conv1 = nn.Conv3d(2, self.num_features, kernel_size=4, stride=2, padding=1)

        self.conv2 = nn.Conv3d(
            self.num_features, 2 * self.num_features, kernel_size=4, stride=2, padding=1
        )
        self.bn2 = nn.BatchNorm3d(2 * self.num_features)

        self.conv3 = nn.Conv3d(
            2 * self.num_features,
            4 * self.num_features,
            kernel_size=4,
            stride=2,
            padding=1,
        )
        self.bn3 = nn.BatchNorm3d(4 * self.num_features)

        self.conv4 = nn.Conv3d(
            4 * self.num_features, 8 * self.num_features, kernel_size=4, stride=1
        )
        self.bn4 = nn.BatchNorm3d(8 * self.num_features)

        # 2 Bottleneck layers
        self.bottleneck = nn.Sequential(
            nn.Linear(self.num_features * 8, self.num_features * 8),
            nn.ReLU(),
            nn.Linear(self.num_features * 8, self.num_features * 8),
            nn.ReLU(),
        )

        # 4 Decoder layers
        self.relu = nn.ReLU()

        self.convt1 = nn.ConvTranspose3d(
            self.num_features * 8 * 2, self.num_features * 4, kernel_size=4, stride=1
        )
        self.bnt1 = nn.BatchNorm3d(4 * self.num_features)

        self.convt2 = nn.ConvTranspose3d(
            self.num_features * 4 * 2,
            self.num_features * 2,
            kernel_size=4,
            stride=2,
            padding=1,
        )
        self.bnt2 = nn.BatchNorm3d(2 * self.num_features)

        self.convt3 = nn.ConvTranspose3d(
            self.num_features * 2 * 2,
            self.num_features,
            kernel_size=4,
            stride=2,
            padding=1,
        )
        self.bnt3 = nn.BatchNorm3d(self.num_features)

        self.convt4 = nn.ConvTranspose3d(
            self.num_features * 2, 1, kernel_size=4, stride=2, padding=1
        )

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        b = x.shape[0]

        # Encode
        # Pass x though encoder while keeping the intermediate
        # outputs for the skip connections
        x_e1 = self.leaky_relu(self.conv1(x))
        x_e2 = self.leaky_relu(self.bn2(self.conv2(x_e1)))
        x_e3 = self.leaky_relu(self.bn3(self.conv3(x_e2)))
        x_e4 = self.leaky_relu(self.bn4(self.conv4(x_e3)))

        # Reshape and apply bottleneck layers
        x = x_e4.view(b, -1)
        x = self.bottleneck(x)
        x = x.view(x.shape[0], x.shape[1], 1, 1, 1)

        # Decode
        # Pass x through the decoder, applying the skip connections in the process
        x_d1 = torch.cat([x_e4, x], dim=1)
        x_d1 = self.relu(self.bnt1(self.convt1(x_d1)))

        x_d2 = torch.cat([x_d1, x_e3], dim=1)
        x_d2 = self.relu(self.bnt2(self.convt2(x_d2)))

        x_d3 = torch.cat([x_d2, x_e2], dim=1)
        x_d3 = self.relu(self.bnt3(self.convt3(x_d3)))

        x_d4 = torch.cat([x_d3, x_e1], dim=1)
        x_d4 = self.convt4(x_d4)
        x = x_d4

        x = torch.squeeze(x, dim=1)

        # Log scaling
        x = torch.log(torch.abs(x) + 1)
        return x

    def _infer_single(self, input_sdf, target_df, truncation_distance=3) -> t.Any:
        """
        Reconstruct a full shape given a partial observation
        :param input_sdf: Input grid with partial SDF of shape 32x32x32
        :param target_df: Target grid with complete DF of shape 32x32x32
        :return: Tuple with mesh representations of input, reconstruction, and target
        """

        # Pass input in the right format though the network
        # and revert the log scaling by applying exp and subtracting 1
        reconstructed_df = self.forward(input_sdf.unsqueeze(0))
        reconstructed_df = torch.exp(reconstructed_df) - 1

        input_mesh = measure.marching_cubes(input_sdf[0].cpu().numpy(), level=1)
        reconstructed_mesh = measure.marching_cubes(
            reconstructed_df.squeeze(0).cpu().numpy(), level=1
        )
        target_mesh = measure.marching_cubes(target_df.cpu().numpy(), level=1)

        return input_mesh, reconstructed_mesh, target_mesh

    @staticmethod
    def _extract_vertices_and_faces(
        mesh, flip_axes=False
    ) -> t.Tuple[np.ndarray, np.ndarray]:

        vertices, faces = mesh[:2]
        if flip_axes:
            vertices[:, 2] = vertices[:, 2] * -1
            vertices[:, [0, 1, 2]] = vertices[:, [0, 2, 1]]

        return vertices.astype(np.float32), faces.astype(np.int32)

    # REGION PYTORCH LIGHTNING
    def configure_optimizers(self):
        optimizer = torch.optim.Adam(params=self.parameters(), lr=self.learning_rate)
        scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=20, gamma=0.5)

        return [optimizer], [scheduler]

    def training_step(self, batch: t.Dict[str, torch.Tensor], batch_idx):
        reconstruction = self.forward(batch["input_sdf"])

        # Mask out known regions -- only use loss on reconstructed,
        # previously unknown regions
        reconstruction[batch["input_sdf"][:, 1] == 1] = 0  # Mask out known
        target = batch["target_df"]
        target[batch["input_sdf"][:, 1] == 1] = 0

        # Compute loss
        loss = self.smooth_mae(reconstruction, target)
        self.log("train_loss", loss, on_epoch=True)

        # Make a scheduler step
        if self.trainer.is_last_batch:
            self.lr_schedulers().step()

        return loss

    def validation_step(self, batch: t.Dict[str, torch.Tensor], batch_idx):
        reconstruction = self.forward(batch["input_sdf"])

        # Transform back to metric space
        # We perform our validation with a pure l1 loss
        # in metric space for better comparability
        reconstruction = torch.exp(reconstruction) - 1
        target = torch.exp(batch["target_df"]) - 1
        # Mask out known regions -- only report loss on reconstructed,
        # previously unknown regions
        reconstruction[batch["input_sdf"][:, 1] == 1] = 0
        target[batch["input_sdf"][:, 1] == 1] = 0

        # calculate losses and validation meshes
        val_loss = self.mae(reconstruction, target).item()

        meshes = self._infer_single(  # [input_mesh, reconstructed_mesh, target_mesh]
            batch["input_sdf"][0],
            batch["target_df"][0],
        )
        input_mesh, reconstructed_mesh, target_mesh = [
            self._extract_vertices_and_faces(mesh) for mesh in meshes
        ]

        # log values
        self.log("val_loss", val_loss)

        tensorboard = self.logger.experiment

        input_vertices, input_faces = input_mesh
        tensorboard.add_mesh(
            "input_mesh", vertices=input_vertices[None], faces=input_faces[None]
        )

        reconstructed_vertices, reconstructed_faces = reconstructed_mesh
        tensorboard.add_mesh(
            "reconstructed_mesh",
            vertices=reconstructed_vertices[None],
            faces=reconstructed_faces[None],
        )

        target_vertices, target_faces = target_mesh
        tensorboard.add_mesh(
            "target_mesh", vertices=target_vertices[None], faces=target_faces[None]
        )

        return val_loss

    def transfer_batch_to_device(
        self,
        batch: t.Dict[str, torch.Tensor],
        device: torch.device,
        dataloader_idx: int,
    ) -> t.Dict[str, torch.Tensor]:
        batch["input_sdf"] = batch["input_sdf"].to(device)
        batch["target_df"] = batch["target_df"].to(device)

        return batch

    # ENDREGION
