import argparse
import os
from multiprocessing import Pool

import numpy as np
import torch
from tqdm import tqdm


def convert_numpy_to_pytorch(model_id):
    try:
        path_to_npz = f"data/sofas/{model_id}/models/sdf_color.npz"
        npz = np.load(path_to_npz)

        negative_sdf = torch.from_numpy(npz["negative_sdf"]).float()
        positive_sdf = torch.from_numpy(npz["positive_sdf"]).float()

        torch.save(
            {
                "negative_sdf": negative_sdf,
                "positive_sdf": positive_sdf,
            },
            f"data/sofas/{model_id}/models/sdf_color.pt",
        )
    except Exception:
        ...


if __name__ == "__main__":
    # arg parser
    parser = argparse.ArgumentParser()
    parser.add_argument("--dataset", type=str, help="Path to dataset")
    parser.add_argument(
        "--num_thread", type=int, help="Number of thread used ot colorize", default=2
    )
    args = parser.parse_args()

    # colorizing
    _, ids, _ = next(os.walk(args.dataset))
    inputs = [model_id for model_id in ids]

    with Pool(args.num_thread) as pool:
        results = list(
            tqdm(
                pool.imap_unordered(convert_numpy_to_pytorch, inputs), total=len(inputs)
            )
        )
