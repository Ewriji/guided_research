import argparse
from configparser import ConfigParser
from contextlib import contextmanager
from itertools import islice

import numpy as np
import torch
from torch.utils.data import DataLoader
from tqdm import tqdm

from src.cnpm.data import ColoredShapeNetDataModule
from src.cnpm.model import ColorNPM
from src.cnpm.training.trainer import (
    construct_typed_config as cnpm_construct_typed_config,
)


class MockTrainer:
    @property
    def is_last_batch(self):
        return False

    @property
    def current_epoch(self):
        return 200


@contextmanager
def timeit():
    def get_elapsed_time():
        end.record()
        torch.cuda.synchronize()
        return start.elapsed_time(end)

    start = torch.cuda.Event(enable_timing=True)
    end = torch.cuda.Event(enable_timing=True)
    start.record()

    yield get_elapsed_time


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--n", type=int, default=1)
    parser.add_argument("--device", type=str, default="cpu")
    args = parser.parse_args()

    # load datamodule
    ini_config = ConfigParser()
    path_to_config = f"./src/cnpm/train_config.ini"
    ini_config.read(path_to_config)

    # Unpack config to typed config class
    config = cnpm_construct_typed_config(ini_config)
    device = torch.device(args.device)

    if args.device.startswith("cuda"):
        torch.multiprocessing.set_start_method("spawn")

    datamodule = ColoredShapeNetDataModule(
        batch_size=config.batch_size,
        num_workers=config.num_workers,
        num_samples_point=config.num_samples_point,
        path_to_split=(
            config.path_to_overfit_split
            if config.is_overfit
            else config.path_to_train_split
        ),
        path_to_dataset=config.path_to_dataset,
    )
    datamodule.setup()

    # Instantiate model
    model = ColorNPM(
        color_suits_num=datamodule.num_color_shifts,
        shape_latent_num=datamodule.train_dataset.shape_num,
        shape_latent_dim=config.shape_latent_code_length,
        color_latent_num=datamodule.train_dataset.color_num,
        color_latent_dim=config.color_latent_code_length,
        model_learning_rate=config.learning_rate_model,
        latent_code_learning_rate=config.learning_rate_code,
        lambda_code_regularization=config.lambda_code_regularization,
    ).to(device)
    model.train_shape = False
    model.train_color = True

    # Optimizers
    (opt,), _ = model.configure_optimizers()

    # Benchmark
    reading_times = []
    transfer_times = []
    forward_times = []
    backward_times = []

    dataloader = DataLoader(
        datamodule.train_dataset,
        batch_size=datamodule.batch_size,
        num_workers=config.num_workers,
        shuffle=True,
        pin_memory=True,
    )

    reading_start = torch.cuda.Event(enable_timing=True)
    reading_end = torch.cuda.Event(enable_timing=True)

    reading_start.record()
    for batch in tqdm(islice(dataloader, args.n), total=args.n):
        # reading data
        reading_end.record()
        reading_times.append(reading_start.elapsed_time(reading_end))

        # transfer data to cuda
        with timeit() as t:
            batch = model.transfer_batch_to_device(batch, device, 1)

        transfer_times.append(t())

        # forward path
        with timeit() as t:
            loss = model.training_step(batch, 1)
        forward_times.append(t())

        # backward path
        with timeit() as t:
            loss.backward()
            opt.step()
            opt.zero_grad()
        backward_times.append(t())

        reading_start.record()

    print(f"Avg loading time: {np.mean(reading_times)} ms")
    print(f"Avg transfer time: {np.mean(transfer_times)} ms")
    print(f"Avg train step time: {np.mean(forward_times)} ms")
    print(f"Avg backward step time: {np.mean(backward_times)} ms")
