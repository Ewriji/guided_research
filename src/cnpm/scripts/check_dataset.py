import argparse
import os
from pathlib import Path

if __name__ == "__main__":
    # arg parser
    parser = argparse.ArgumentParser()
    parser.add_argument("--dataset", type=str, help="Path to dataset")
    parser.add_argument("--split", type=str, help="Path to split")
    args = parser.parse_args()

    # colorizing
    _, dataset_ids, _ = next(os.walk(args.dataset))
    split_ids = Path(args.split).read_text().splitlines()

    for item_id in split_ids:
        check_path = Path(args.dataset) / item_id / "models" / "m_colors.npz"
        if not check_path.exists():
            print(f"Shape: {item_id} is invalid!")
