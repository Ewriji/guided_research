import argparse
import os
from multiprocessing import Pool

import cv2
import numpy as np
from tqdm import tqdm

from src.util.misc import remove_nans


def colorize_sample(input):
    try:
        model_id, dataset_path, num_shifts = input

        npz = np.load(f"{dataset_path}/{model_id}/models/sdf_color.npz")
        pos_tensor = remove_nans(npz["positive_sdf"], is_torch=False)
        neg_tensor = remove_nans(npz["negative_sdf"], is_torch=False)

        samples = np.concatenate([pos_tensor, neg_tensor], 0)

        points = samples[:, :3]
        sdf = samples[:, 3]
        colors = samples[:, 4:]
        hsv = cv2.cvtColor(colors[:, None, :], cv2.COLOR_BGR2HSV)

        colors_with_positive_sdf = []
        colors_with_negative_sdf = []

        negative_sdf_mask = sdf < 0
        positive_sdf_mask = ~negative_sdf_mask

        shift = 360 // num_shifts
        for i in range(num_shifts):
            hsv[:, :, 0] += shift
            rgb = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR).squeeze()

            # prepare colors
            colors_with_positive_sdf.append(
                rgb[positive_sdf_mask],
            )
            colors_with_negative_sdf.append(
                rgb[negative_sdf_mask],
            )

        colors_with_positive_sdf = np.array(colors_with_positive_sdf)
        colors_with_negative_sdf = np.array(colors_with_negative_sdf)

        # save npz
        np.savez_compressed(
            f"{dataset_path}/{model_id}/models/m_colors",
            psdf_values=sdf[positive_sdf_mask],
            nsdf_values=sdf[negative_sdf_mask],
            psdf_points=points[positive_sdf_mask],
            nsdf_points=points[negative_sdf_mask],
            psdf_colors=colors_with_positive_sdf,
            nsdf_colors=colors_with_negative_sdf,
        )
    except Exception:
        ...


if __name__ == "__main__":
    # arg parser
    parser = argparse.ArgumentParser()
    parser.add_argument("--dataset", type=str, help="Path to dataset")
    parser.add_argument(
        "--num_shifts", type=int, help="Number of color shifts to make", default=20
    )
    parser.add_argument(
        "--num_thread", type=int, help="Number of thread used ot colorize", default=2
    )
    args = parser.parse_args()

    # colorizing
    _, ids, _ = next(os.walk(args.dataset))
    inputs = [(model_id, args.dataset, args.num_shifts) for model_id in ids]

    with Pool(args.num_thread) as pool:
        results = list(
            tqdm(pool.imap_unordered(colorize_sample, inputs), total=len(inputs))
        )
